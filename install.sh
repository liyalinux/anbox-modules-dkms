#!/usr/bin/env bash

# First install the configuration files:
sudo cp anbox.conf /etc/modules-load.d/
sudo cp 99-anbox.rules /usr/lib/udev/rules.d/

# Then copy the module sources to /usr/src/:
sudo cp -rT ashmem /usr/src/ashmem-1
sudo cp -rT binder /usr/src/binder-1

# Finally use dkms to build and install:
echo "==>> Building Modules (ashmem)"
cp -rf /usr/src/ashmem-1 /tmp/ashmem_install
cd /tmp/ashmem_install
make
sudo make DESTDIR="/usr/src/ashmem-1" install
echo "==>> Building Modules (binder)"
cp -rf /usr/src/binder-1 /tmp/binder_install
cd /tmp/binder_install
make
sudo make DESTDIR="/usr/src/binder-1" install
echo "==>> Cleaning Up"
rm -rf /tmp/ashmem_install
rm -rf /tmp/binder_install

# Load modules
modprobe binder_linux devices=binder,hwbinder,vndbinder,anbox-binder,anbox-hwbinder,anbox-vndbinder
modprobe ashmem_linux
