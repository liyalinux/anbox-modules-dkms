# Anbox Kernel Modules

This repository contains the kernel modules necessary to run the Anbox
Android container runtime. They're split out of the original Anbox
repository to make packaging in various Linux distributions easier.

# Install Instructions

You need to have `dkms` and linux-headers on your system. You can install them by
`sudo apt install dkms` or `sudo yum install dkms` (`dkms` is available in epel repo
for CentOS) or `sudo pacman -S dkms` for arch linux based distributions.

Package name for linux-headers varies on different distributions, e.g.
`linux-headers-generic` (Ubuntu), `linux-headers-amd64` (Debian),
`linux-headers` (Arch) `kernel-devel` (CentOS, Fedora), `kernel-default-devel` (openSUSE).

Run 'sudo ./install.sh' to install the anbox-modules
